"use strict";/**
* Develop by katapema@yahoo.com
*/

/**
* Shorthand to access document, window and Element's prototype object. The variable is useful to getElementById and other valuable DOM method
*/
var $d = document,
	$w = window,
	$$E = Element.prototype,
	$$D = DocumentFragment.prototype;

/**
* Shorthand to access common selector method:
*   - document.getElementById() is replaced by $id()
*   - o.getElementsByClassName() is replaced by o.$class()
*   - o.getElementsByTagName() is replaced by o.$tag()
*   - o.querySelector() is replaced by o.$query()
*   - o.querySelectorAll() is replaced by o.$queryAll()
*/
function $id(e) {
	return document.getElementById(e);
}

$d.$class = $d.getElementsByClassName;
$$E.$class = $$E.getElementsByClassName;

$d.$tag = $d.getElementsByTagName;
$$E.$tag = $$E.getElementsByTagName;

$d.$query = $d.querySelector;
$$E.$query = $$E.querySelector;

$d.$queryAll = $d.querySelectorAll;
$$E.$queryAll = $$E.querySelectorAll;

/**
* @param e Element to be matched with selector
* @param s Selector to be matched
* @param p Parent element of matching element
* @returns true if e fall into a child node of parent node and having the selector.
*          Otherwise false
*/
function $fitParentSelector(e, s, p) {
	var i, t = e,
		l = p.$queryAll(s);
	while (t != p) {
		for (i=l.length-1; i>=0; i--)
			if (l[i] == t)
				return t;
		t = t.parentNode;
	}
	return null;
}

$$E.$addCls = function (c) {
	this.classList.add(c);
}

$$E.$addClses = function (l) {
	l.forEach((c) => {
		this.classList.add(c);
	});
}

$$E.$rmCls = function (c) {
	this.classList.remove(c);
}

$$E.$clrChild = function () {
	while (this.firstChild)
		this.removeChild(this.firstChild);
}

$$E.$prepend = function (e) {
	e = $d.createElement(e);
	this.prepend(e);
	return e;
}

$$E.$prependTxtContent = function (e, t) {
	e = $d.createElement(e);
	this.prepend(e);
	e.textContent = t;
	return e;
}

$$E.$prependTxtNode = function (t) {
	this.prepend($d.createTextNode(t));
}

$$E.$add = function (e) {
	e = $d.createElement(e);
	this.appendChild(e);
	return e;
}

$$E.$addTxtContent = function (e, t) {
	e = $d.createElement(e);
	this.appendChild(e);
	e.textContent = t;
	return e;
}

$$E.$addTxtNode = function (t) {
	this.appendChild($d.createTextNode(t));
}

$$D.$add = function (e) {
	e = $d.createElement(e);
	this.appendChild(e);
	return e;
}

function $NL2BR(s) {
	return s.replace(/\r\n|\n\r|\r|\n/g, '<br/>');
}

$w.addEventListener('load', function () {
	try {
		new Main();
	}
	catch (e) {
		console.log(e);
	}
});function $getIDByCls(e) {
	let i = 0,
		l = e.classList;
	for (i = l.length - 1; i >= 0; i--)
		if (l.item(i).startsWith('id'))
			return l.item(i).substr(2);
	return 0;
}function $$rm(e) {
	e.parentNode.removeChild(e);
}

function $rm(e) {
	if (e instanceof NodeList)
		for (var i=e.length-1; i>=0; i--)
			$$rm(e[i]);
	else
		$$rm(e);
}function $After(n, e) {
	var p = n.parentNode;
	if (n.nextSibling)
		p.insertBefore(e, n.nextSibling);
	else
		p.appendChild(e);
}Number.prototype.FormatMoney = function(c, d, t) {
	var n = this, 
	c = isNaN(c = Math.abs(c)) ? 2 : c, 
	d = d == undefined ? "." : d, 
	t = t == undefined ? "," : t, 
	s = n < 0 ? "-" : "", 
	i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
	j = (j = i.length) > 3 ? j % 3 : 0;
	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}
 
Number.prototype.leadingZero = function(c) {
	var s = this + '';
	while (s.length < c)
		s = '0' + s;
	return s;
}
function $Before(n, e) {
	n.parentNode.insertBefore(e, n);
}/*
* Developed by Tran Anh Dung
* Contact: katapema@yahoo.com
*
* Require: Core.js
*/

/*
* GET request
* u: URL
* c: callback function when request is successfully fetched
* e: error callback function when request is failed
* r: 'raw' to get the raw data. Otherwise JSON is passed to callback function
*/
function $get(u, c, e, r) {
	var p = new XMLHttpRequest();
	p.open('GET', u, true);

	p.onreadystatechange = function() {
		if (p.readyState == 4)
			if (p.status == 200) {
				if (c)
					if (r == 'raw')
						c(p.responseText, p);
					else {
						var t = null;
						try {
							t = JSON.parse(p.responseText);
						}
						catch (t) {
							if (e)
								e();
						}
						if (t != null)
							c(t);
					}
			}
			else if (e)
				e();
	};
	p.send();
	return p;
}

/*
* POST request
* u: URL
* d: data to POST or FORM element
* c: callback function when request is successfully fetched
* e: error callback function when request is failed
* r: 'raw' to get the raw data. Otherwise JSON is passed to callback function
*/
function $post(u, d, c, e, r) {
	var p = new XMLHttpRequest(),
	s = '',
	a, t, m, n, j, o, i;

	p.open('POST', u, true);

	function V(n, v) {
		if (Array.isArray(v)) {
			v.forEach(function(e) {
				s += (s.length > 0 ? '&' : '')
					+ encodeURI(n).replace(/\+/g, '%2B') + '='
					+ encodeURIComponent(e ? e : (e === 0 ? '0' : '')).replace(/\+/g, '%2B');
			});
		}
		else {
			s += (s.length > 0 ? '&' : '')
					+ encodeURI(n).replace(/\+/g, '%2B') + '='
					+ encodeURIComponent(v ? v : (v === 0 ? '0' : '')).replace(/\+/g, '%2B');
		}
	}

	if (d instanceof HTMLFormElement) {
		p.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
		m = d.elements;
		for (i=m.length-1; i>=0; i--) {
			a = m[i];
			t = a.type.toUpperCase();
			n = a.name;
			if (n) {
				if (  t == 'TEXT' || t == 'TEXTAREA' || t == 'PASSWORD'
				   || t == 'BUTTON' || t == 'RESET' || t == 'SUBMIT'
				   || t == 'FILE' || t == 'IMAGE' || t == "HIDDEN")
					V(n, a.value);
				else if (t == 'CHECKBOX' && a.checked)
					V(n, a.value ? a.value : 'on');
				else if (t == 'RADIO' && a.checked)
					V(n, a.value);
				else if (t.indexOf('SELECT') != -1)
					for (j=0; j<a.options.length; j++) {
						o = a.options[j];
						if (o.selected)
							V(n, o.value ? o.value : o.text);
					}
			}
		}
	}
	else {
		var t = false;
		for (a in d)
			if (d[a] instanceof Blob) {
				t = true;
				break;
			}
		if (t) {
			s = new FormData();
			for (a in d)
				if (d[a] instanceof Blob)
					s.append(a, d[a], 'file.blob');
				else
					s.append(a, d[a]);
		}
		else {
			p.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
			for (a in d)
				V(a, d[a]);
		}
	}

	p.onreadystatechange = function() {
		if (p.readyState == 4)
			if (p.status == 200) {
				if (c)
					if (r == 'raw')
						c(p.responseText, p);
					else {
						var t = null;
						try {
							t = JSON.parse(p.responseText);
						}
						catch (t) {
							if (e != undefined && e != null)
								e();
						}
						if (t != null)
							c(t);
					}
			}
			else if (e != undefined && e != null)
				e();
	};
	p.send(s);
	return p;
}
$$E.$on = $$E.addEventListener;
$$E.$off = $$E.removeEventListener;
WebSocket.prototype.$on = WebSocket.prototype.addEventListener;var $iTimer, $iInterval, $pTimer;
$iTimer = 0;
$iInterval = 40;
$pTimer = new Array();

function $SetInterval(t, c)
{
	if (!$iTimer)
	{
		$iTimer = setInterval(function()
		{
			var p;
			var n = (new Date()).getTime();
			for (var i=$pTimer.length-1; i>=0; i--)
			{
				p = $pTimer[i];
				if (p.t < n - p.l)
				{
					p.l = n;
					p.c();
				}
			}
		}, $iInterval);
	}
	var p = new Object();
	p.l = (new Date()).getTime();
	p.t = t;
	p.c = c;
	$pTimer.push(p);
	return p;
}

function $ClrInterval(t)
{
	for (var i=$pTimer.length-1; i>=0; i--)
	{
		if ($pTimer[i] == t)
		{
			$pTimer.splice(i, 1);
			break;
		}
	}
	if ($pTimer.length == 0)
	{
		clearInterval($iTimer);
		$iTimer = 0;
	}
}
/*
* Developed by Tran Anh Dung
* Contact: katapema@yahoo.com
*
* Require: Timer.js
*/

function $FadeIn(e, t, c)
{
	var i, m,
	d = 40/t,
	o = 0,
	p = new Date();

	i = $SetInterval(40, function()
	{
		m = ((new Date()).getTime() - p.getTime())/t;
		o += d*m;
		if (o > 1)
		{
			e.style.opacity = 1;
			e.style.filter = 'alpha(opacity=100)';
			$ClrInterval(i);
			if (c)
			{
				c();
				c = null;
			}
		}
		else
		{
			e.style.opacity = o;
			e.style.filter = 'alpha(opacity=' + parseInt(o*100) + ')';
		}
	});
	return i;
}

function $FadeOut(e, t, c)
{
	var i, m,
	d = 40/t,
	o = e.style.opacity == '' ? 1 : e.style.opacity,
	p = new Date();

	if (typeof o === 'undefined')
	{
		o = e.style.filter.substr(14);
		o = o == '' ? 100 : parseInt(o);
		o = o/100;
	}
	i = $SetInterval(40, function()
	{
		m = ((new Date()).getTime() - p.getTime())/t;
		o -= d*m;
		if (o < 0)
		{
			e.style.opacity = 0;
			e.style.filter = 'alpha(opacity=0)';
			$ClrInterval(i);
			if (c)
			{
				c();
				c = null;
			}
		}
		else
		{
			e.style.opacity = o;
			e.style.filter = 'alpha(opacity=' + parseInt(o*100) + ')';
		}
	});
	return i;
}/**
* Develope by katapema@yahoo.com
*
* Require: Core.js, Event/Core.js
* Usage:
* - Create a FormGeneral object: form = new $FormGeneral(f, cb);
* ++ f: form element. This can be done by using $id('form-id')
* ++ cb: callback object to process form event
* ++++ cb.beforePost: happen before form post. Return true if want to proceed with post.
*                     Otherwise, form will stop posting
* ++++ cb.onResult: happen when post return result
*/
class $FormGeneral {
	constructor(f, cb) {
		this.form = f;
		this.cb = cb;
		this.btnSubmit = f.$query('.submit');
		this.eSubmit = f.$query('.eSubmit');
		this.errs = f.$queryAll('.err');
		this.submitDelayTime = null;

		var self = this;
		this.form.$on('submit', function (e) {self.zOnSubmit(e);});
	}

	clrErr() {
		var errs = this.errs,
			i = errs.length-1,
			e;
		for (; i>=0; i--) {
			e = errs[i];
			e.classList.add('hide');
		}
	}

	submit() {
		this.clrErr();
		this.zSetSubmitLoading();

		var self = this;
		$post(self.form.action, self.form,
			function (r) {
				var t = new Date();
				t = self.submitDelayTime.getTime() - t.getTime();
				if (t > 0)
					setTimeout(function(){self.zOnPostResult(r)}, t);
				else
					self.zOnPostResult(r);
			},
			function () {
				var t = new Date();
				t = self.submitDelayTime.getTime() - t.getTime();
				if (t > 0)
					setTimeout(function(){self.zOnPostErr()}, t);
				else
					self.zOnPostErr();
			});
	}

	zOnSubmit(e) {
		e.preventDefault();
		if (this.cb.beforePost != undefined)
			if (!this.cb.beforePost())
				return;

		this.submit();
	}

	zOnPostErr() {
		this.zSetSubmitNormal();
		this.zSetErr(this.eSubmit, "Unable to connect to server. Please try again later. If the issue is still persist, please inform web admin. Thank you!");
	}

	zOnPostResult(r) {
		this.zSetSubmitNormal();
		if (r.r == undefined)
			return;
		if (r.r != 0)
			this.zOnSubmitErr(r);
		else if (this.cb && this.cb.onResult)
			this.cb.onResult(r);
	}

	zSetErr(c, e) {
		c.textContent = e;
		c.classList.remove('hide');
	}

	zSetSubmitLoading() {
		var btn = this.btnSubmit;
		btn.setAttribute('disabled', 'disabled');
		btn.$add('i').className = 'fa fa-spinner fa-spin';
		this.submitDelayTime = new Date((new Date()).getTime() + 300);
	}

	zSetSubmitNormal() {
		var btn = this.btnSubmit;
		btn.classList.remove('disable');
		btn.removeAttribute('disabled');
		btn.removeChild(btn.lastChild);
	}

	zOnSubmitErr(e) {
		var key, val;
		for (key in e) {
			if (key == 'r')
				continue;
			val = e[key];
			if (key.startsWith('e'))
				this.zSetErr(this.form.$query('.' + key), val);
		};
	}
}
/*
* Develope by KKode Pte Ltd.
* Contact: developer@kkode.com
*
* Require: Core.js, Event/Core.js, Form/FormGeneral
* Usage:
* - Create a FormSearch object: form = new $FormSearch(s, r, c, p, cb);
* ++ f: form element
* ++ r: result panel element
* ++ c: control element to show number of record found
* ++ p: paging element
* ++ cb: callback object to process some event
* ++++ cb.beforePost: happen before form post. Return true if want to proceed with post.
*                     Otherwise, form will stop posting
* ++++ cb.onResult: happen when post return result
* ++++ cb.onClkPage: happen when clicking on page in result panel
*/
class $FormSearch {
	constructor(f, r, c, p, cb) {
		this.form = new $FormGeneral(f, this);
		this.result = r;
		this.count = c;
		this.paging = p;
		this.cb = cb;
		this.page = f.elements['Page'];

		this.count.classList.add('hide');
		this.paging.classList.add('hide');
	}

	submit() {
		this.form.submit();
	}

	beforePost() {
		this.count.classList.add('hide');
		this.paging.classList.add('hide');
		this.page.value = 0;
		this.zSetResultLoading();
		if (this.cb.beforePost)
			return this.cb.beforePost();
		else
			return true;
	}

	onResult(r) {
		this.zSetCount(r.total);
		if ('page' in r)
			this.page.value = r.page;
		this.zSetPaging(r.total, r.itemPerPage);

		if (r.total == 0)
			this.result.classList.add('hide');
		else {
			this.result.classList.remove('hide');
			if (this.cb.onResult)
				this.cb.onResult(r);
		}
	}

	onClkPage(p) {
		if (this.cb.onClkPage)
			if (!this.cb.onClkPage(p))
				return;
		this.zSetResultLoading();
		this.page.value = p;
		this.form.submit();
	}

	getPage(page) {
		return parseInt(this.page.value);
	}

	zSetResultLoading() {
		this.result.$clrChild();
		this.result.$add('i').className = 'fa fa-spinner fa-spin';
	}

	zSetCount(total) {
		this.count.classList.remove('hide');
		if (total == 0)
			this.count.textContent = 'No record found';
		else if (total == 1)
			this.count.textContent = 'Found 1 record';
		else
			this.count.textContent = 'Found ' + total + ' records';
	}

	zSetPaging(total, itemPerPage) {
		var p = new $Paging({
			page: this.getPage(),
			totalPage: Math.ceil(total / itemPerPage),
			cb: this
		});

		if (p.getDOM()) {
			if (this.paging.firstChild)
				this.paging.removeChild(this.paging.firstChild);
			this.paging.appendChild(p.getDOM());
			this.paging.classList.remove('hide');
		}
		else
			this.paging.classList.add('hide');
	}
}
/*
* Develope by KKode Pte Ltd.
* Contact: developer@kkode.com
*
* Require: Core.js, Event/Core.js, Form/FormGeneral
* Usage:
* - Create a FormAdd object: form = new $FormAdd(s, cb);
* ++ s: form id selector
* ++ cb: callback object to process some event
* ++++ cb.beforePost: happen before form post. Return true if want to proceed with post.
*                     Otherwise, form will stop posting
* ++++ cb.onResult: happen when post return result
*/
class $FormAdd extends $FormGeneral {
	constructor(s, cb) {
		super(s, cb);
	}
}
class $Dialog {
	constructor(d, o) {
		this.dlg = d;
		this.overlay = o;
	}

	show() {
		this.overlay.classList.remove('hide');
		this.dlg.classList.remove('hide');
	}

	hide() {
		this.overlay.classList.add('hide');
		this.dlg.classList.add('hide');
	}
}
window.addEventListener('load', function(e) {
	var sidebarTrigger = $id('sidebarTrigger'),
		sidebar = $id('sidebar'),
		content = $id('content'),
		sidebarmin = $id('sidebarmin');
	sidebarTrigger.$on('touchstart', function(e) {
		e.preventDefault();
		sidebar.classList.toggle('sidebarhide');
		content.classList.toggle('moveright');
		sidebarTrigger.classList.toggle('rotate');
	});
	sidebarmin.$on('touchstart', function(e) {
		sidebar.classList.toggle('min');
		if (sidebar.classList.contains('min'))
			$CookieSet('sidebarmin', '1', 60*60*24*365*1000, '/');
		else
			$CookieDel('sidebarmin', '/');
	});
	sidebarTrigger.$on('click', function() {
		sidebar.classList.toggle('sidebarhide');
		content.classList.toggle('moveright');
		sidebarTrigger.classList.toggle('rotate');
	});
	sidebarmin.$on('click', function() {
		sidebar.classList.toggle('min');
		if (sidebar.classList.contains('min'))
			$CookieSet('sidebarmin', '1', 60*60*24*365*1000, '/');
		else
			$CookieDel('sidebarmin', '/');
	});
});
class DlgAlert {
	constructor() {
		this.overlay = $id('overlay');
		this.dlg = $id('dlgAlert');
		this.msg = this.dlg.$query('.msg');
		this.btnOk = this.dlg.$query('.btnOk');
		this.btnCancel = this.dlg.$query('.btnCancel');
		this.fOk = null;

		var self = this;
		this.btnOk.$on('click', function(e) {self.zOnClkBtnOk(e);});
		this.btnCancel.$on('click', function(e) {self.zOnClkBtnCancel(e);});
	}

	showConfirm(m, fOk) {
		this.msg.textContent = m;
		this.fOk = fOk;
		this.btnCancel.classList.remove('hide');
		this.dlg.classList.remove('hide');
		this.overlay.classList.remove('hide');
	}

	showAlert(m) {
		this.msg.textContent = m;
		this.fOk = null;
		this.btnCancel.classList.add('hide');
		this.dlg.classList.remove('hide');
		this.overlay.classList.remove('hide');
	}

	zOnClkBtnOk(e) {
		e.preventDefault();
		this.dlg.classList.add('hide');
		this.overlay.classList.add('hide');
		if (this.fOk != null)
			this.fOk();
	}

	zOnClkBtnCancel(e) {
		e.preventDefault();
		this.dlg.classList.add('hide');
		this.overlay.classList.add('hide');
	}
}

var dlgAlert = new DlgAlert;class Notif {
	constructor() {
		this.notif = $id('notif');
		this.msg = this.notif.$query('.msg');
		this.notifTimeout = 0;
		this.notifFadeOut = 0;
	}

	show(m, d, f) {
		if (this.notifTimeout != 0) {
			clearTimeout(this.notifTimeout);
			this.notifTimeout = 0;
		}
		if (this.notifFadeOut != 0) {
			clearInterval(this.notifFadeOut);
			this.notifFadeOut = 0;
		}
		this.msg.textContent = m;
		this.notif.classList.remove('hide');
		this.notif.style.opacity = 1;
		this.notif.style.filter = 'alpha(opacity=100)';
		if (d > 0) {
			this.notifTimeout = setTimeout(() => {
				this.notifTimeout = 0;
				this.notifFadeOut = $FadeOut(this.notif, 1000, () => {
					this.notifFadeOut = 0;
					this.notif.classList.add('hide');
					if (f)
						f();
				});
			}, d);
		}
	}
}

let notif = new Notif;/*
* Develope by KKode Pte Ltd.
* Contact: developer@kkode.com
*
* Require: Core.js, Event/Core.js
* Usage:
* - Create a calendar object: calendar = new $Calendar(o);
* ++ o: option for calendar
* ++++ o.days: text array to show day name (default: ['S', 'M', 'T', 'W', 'T', 'F', 'S'])
* ++++ o.months: text array to show month name (default: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'])
* ++++ o.namespace: namespace added into class attribute of main control
* ++++ o.cb: callback object for event management, the callback should implement these functions
* ++++++++ onSelDate(selDate): when a date is seleted
* - Add datepicker object into DOM document: $After(pBtnCancelFrom, pDPFrom.GetDOM());
*/
class $Calendar {
	constructor(o) {
		this.zResetValue();
		this.zParseOption(o);
		this.zBuildDOM();
	}

	getDOM() {
		return this.dom;
	}

	clear() {
		this.date.$query('.sel').classList.remove('sel');
	}

	getSelDate() {
		return this.selDate;
	}

	setSelDate(y, m, d) {
		m = m - 1;
		this.selDate = new Date(y, m, d);
		this.m = m;
		this.y = y;
		this.refresh();
	}

	setSelToday() {
		var t = new Date();
		this.setSelDate(t.getFullYear(), t.getMonth()+1, t.getDate());
	}

	setMonth(y, m) {
		this.m = m-1;
		this.y = y;
		this.zBuildDate();
	}

	refresh() {
		this.title.textContent = this.months[this.m] + ' ' + this.y;
		this.zBuildDate();
	}

	show() {
		this.dom.classList.remove('hide');
	}

	hide() {
		this.dom.classList.add('hide');
	}

	isFocus() {
		return this.bFocus;
	}

	zResetValue() {
		this.days         = ['S', 'M', 'T', 'W', 'T', 'F', 'S'];
		this.months       = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
		this.selDate      = null;
		this.namespace    = null;

		this.cb           = null;

		this.dom = null;
		this.title = null;
		this.date = null;
		var d = new Date();
		this.m = d.getMonth();
		this.y = d.getFullYear();
	}

	zParseOption(o) {
		if ('days'         in o) this.days         = o.days;
		if ('months'       in o) this.months       = o.months;
		if ('namespace'    in o) this.namespace    = o.namespace;
		if ('cb'           in o) this.cb           = o.cb;
	}

	zBuildDOM() {
		var i, hdr, item, t, table, tbody, tr, self = this,
		d = document.createElement('div');
		d.setAttribute('tabindex', '-1');
		d.className = 'calendar' + (this.namespace ? ' ' + this.namespace : '');
		hdr = d.$add('div');
		hdr.className = 'header';
		item = hdr.$add('div');
		item.className = 'prvYear';
		t = item.$add('i');
		t.className = 'fa fa-angle-double-left';
		t.setAttribute('aria-hidden', 'true');

		item = hdr.$add('div');
		item.className = 'prvMonth';
		t = item.$add('i');
		t.className = 'fa fa-chevron-left';
		t.setAttribute('aria-hidden', 'true');

		this.title = hdr.$add('div');
		this.title.className = 'title';
		this.title.textContent = this.months[this.m] + ' ' + this.y;

		item = hdr.$add('div');
		item.className = 'nxtMonth';
		t = item.$add('i');
		t.className = 'fa fa-chevron-right';
		t.setAttribute('aria-hidden', 'true');

		item = hdr.$add('div');
		item.className = 'nxtYear';
		t = item.$add('i');
		t.className = 'fa fa-angle-double-right';
		t.setAttribute('aria-hidden', 'true');

		table = d.$add('table');
		table.className = 'datelist';
		tbody = table.$add('tbody');
		tr = tbody.$add('tr');
		tr.className = 'days';
		for (i=0; i<this.days.length; i++)
			tr.$addTxtContent('td', this.days[i]);
		this.date = table.$add('tbody');
		this.date.className = 'date';

		this.dom = d;
		this.zBuildDate();

		this.dom.$query('.prvMonth').$on('click', function(e){self.zOnClkPrvMonth(e)});
		this.dom.$query('.nxtMonth').$on('click', function(e){self.zOnClkNxtMonth(e)});
		this.dom.$query('.prvYear').$on('click', function(e){self.zOnClkPrvYear(e)});
		this.dom.$query('.nxtYear').$on('click', function(e){self.zOnClkNxtYear(e)});
		this.date.$on('click', function(e){self.zOnClkDate(e)});
	}

	zBuildWeek(skipFirst, skipLast, firstDate, selDate, tr) {
		var i, td;
		for (i=0; i<skipFirst; i++)
			tr.$add('td');
		for (; i<skipLast; i++, firstDate++) {
			td = tr.$addTxtContent('td', firstDate);
			td.className = 'day';
			if (firstDate == selDate)
				td.className += ' sel';
		}
		for (; i<7; i++)
			tr.$add('td');
	}

	zBuildDate() {
		var i, d=0,
		firstDayOfWeek = (new Date(this.y, this.m, 1)).getDay(),
		totalDays = (new Date(this.y, this.m+1, 0)).getDate(),
		totalWeeks = parseInt((firstDayOfWeek + totalDays + 6) / 7),
		firstWeekLen = 7 - firstDayOfWeek,
		lastWeekLen = totalDays-((totalWeeks-1)*7-firstDayOfWeek),
		tr;
		if (this.selDate != null && this.selDate.getMonth() == this.m && this.selDate.getFullYear() == this.y)
			d = this.selDate.getDate();
		this.date.$clrChild();
		for (i=0; i<totalWeeks; i++) {
			tr = document.createElement('tr');
			if (i == 0)
				this.zBuildWeek(firstDayOfWeek, 7, 1, d, tr);
			else if (i < totalWeeks - 1)
				this.zBuildWeek(0, 7, firstWeekLen + (i-1)*7+1, d, tr);
			else {
				this.zBuildWeek(0, lastWeekLen, firstWeekLen + (i-1)*7+1, d, tr);
			}
			this.date.appendChild(tr);
		}
	}

	zOnClkPrvMonth(e) {
		if (this.m == 0) {
			this.m = 11;
			this.y--;
		}
		else
			this.m--;
		this.refresh();
	}

	zOnClkNxtMonth(e) {
		if (this.m == 11) {
			this.m = 0;
			this.y++;
		}
		else
			this.m++;
		this.refresh();
	}


	zOnClkPrvYear(e) {
		if (this.y > 0)
			this.y--;
		this.refresh();
	}

	zOnClkNxtYear(e) {
		this.y++;
		this.refresh();
	}

	zOnClkDate(e) {
		var t = e.target.classList;
		if (t.contains('day')) {
			var s = this.date.$query('.sel');
			if (s != null)
				s.classList.remove('sel');
			this.selDate = new Date(this.y, this.m, e.target.textContent);
			e.target.classList.add('sel');
			if (this.cb != null && this.cb.onSelDate != null)
				this.cb.onSelDate(this, this.selDate);
		}
	}
}
/*
* Develope by KKode Pte Ltd.
* Contact: developer@kkode.com
*
* Require: Core.js, Event/Core.js, Control/Calendar.js
* Usage:
* - Create a CalendarInput object: calendarInput = new $CalendarInput(o);
* ++ o: option for CalendarInput
* ++++ o.days: text array to show day name (default: ['S', 'M', 'T', 'W', 'T', 'F', 'S'])
* ++++ o.months: text array to show month name (default: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'])
* ++++ o.namespace: namespace added into class attribute of main control
* ++++ o.input: hidden input to bind select date value
*
* - INIT VALUE FOR CALENDAR INPUT
* To init value for input, just set the hidden input value to the desired value. For example
* <input type="hidden" value="20160102"/>
* If hidden input has no value, the CalendarInput will be init with no date selected
*/
class $CalendarInput {
	constructor(o) {
		o.cb = this;

		this.calendar = new $Calendar(o);
		this.input = o.input;

		this.setSelDateYYYYMMDD(this.input.value);
		$After(this.input, this.calendar.getDOM());
		return this;
	}

	setSelToday() {
		this.zSetSelDate(new Date());
	}

	setSelDate(y, m, d) {
		this.zSetSelDate(new Date(y, m-1, d, 0, 0, 0, 0));
	}

	setSelDateYYYYMMDD(yyyymmdd) {
		var y = parseInt(yyyymmdd.substr(0, 4)),
		m = parseInt(yyyymmdd.substr(4, 2)),
		d = parseInt(yyyymmdd.substr(6, 2));

		if (isNaN(y) || isNaN(m) || isNaN(d))
			this.setSelToday();
		else
			this.setSelDate(y, m, d);
	}

	onSelDate(c, d) {
		var m = d.getMonth() + 1,
		t = d.getDate();
		if (m < 10)
			m = '0' + m;
		if (t < 10)
			t = '0' + t;
		this.input.value = '' + d.getFullYear() + m + t;
	}

	zSetSelDate(t) {
		var y = t.getFullYear(),
		m = t.getMonth() + 1,
		d = t.getDate();
		this.zSetInputDate(y, m, d);
		this.calendar.setSelDate(y, m, d);
	}

	zSetInputDate(y, m, d) {
		this.input.value = '' + y + (m < 10 ? '0' + m : m) + (d < 10 ? '0' + d : d);
	}
}
/*
* Develope by KKode Pte Ltd.
* Contact: developer@kkode.com
*
* Require: Core.js, Event/Core.js
* Usage:
* - Create a TimeControl object: tc = new $TimeControl(o);
* ++ o: option for calendar
* ++++ o.namespace: namespace added into class attribute of main control
* ++++ o.cb: callback object for event management, the callback should implement these functions
* ++++++++ onSelTime(selHour, selMinute, selSecond): when a time is selected
* - Add control object into DOM document: $After(pBtnCancelFrom, tc.GetDOM());
*/
class $TimeControl {
	constructor(o) {
		this.zResetValue();
		this.zParseOption(o);
		this.zBuildDOM();
	}

	getDOM() {
		return this.dom;
	}

	getSelTime() {
		return this.selTime;
	}

	setSelTime(y, m, d) {
		m = m - 1;
		this.selDate = new Date(y, m, d);
		this.m = m;
		this.y = y;
		this.refresh();
	}

	setSelNow() {
		var t = new Date();
		this.setSelDate(t.getFullYear(), t.getMonth()+1, t.getDate());
	}

	show() {
		this.dom.classList.remove('hide');
	}

	hide() {
		this.dom.classList.add('hide');
	}

	zResetValue() {
		this.namespace    = null;
		this.cb           = null;

		this.dom          = null;
		this.domHour      = null;
		this.domMin       = null;
		this.domSec       = null;

		this.selTime      = new Date(2000, 1, 1, 0, 0, 0, 0);
	}

	zParseOption(o) {
		if ('namespace'    in o) this.namespace    = o.namespace;
		if ('cb'           in o) this.cb           = o.cb;
	}

	zBuildDOM() {
		var self = this,
		d = document.createElement('div');
		d.className = timecontrol + (this.namespace ? ' ' + this.namespace : '');
		d.setAttribute('tabindex', '-1');
		this.dom = d;
		this.zBuildDOMHour();
		this.zBuildDOMMin();
		this.zBuildDOMSec();

		this.domSelHour = this.dom.$query('.selHour');
		this.domSelMin = this.dom.$query('.selMinute');
		this.domSelSec = this.dom.$query('.selSecond');
		this.domSelHour.$on('change', function(e) { self.zOnSelTime(); });
		this.domSelMin.$on('change', function(e) { self.zOnSelTime(); });
		this.domSelSec.$on('change', function(e) { self.zOnSelTime(); });
	}

	zBuildDOMHour() {
		var i, s,
			d = this.dom.$add('div');
		d.className = 'hour';
		s = d.$add('select');
		s.className = 'selHour';
		for (i=0; i<24; i++)
			s.$addTxtContent('option', i).value = i;
	}

	zBuildDOMMin() {
		var i, s,
			d = this.dom.$add('div');
		d.className = 'minute';
		s = d.$add('select');
		s.className = 'selMinute';
		for (i=0; i<60; i++)
			s.$addTxtContent('option', i).value = i;
	}

	zBuildDOMSec() {
		var i, s,
			d = this.dom.$add('div');
		d.className = 'second';
		s = d.$add('select');
		s.className = 'selSecond';
		for (i=0; i<60; i++)
			s.$addTxtContent('option', i).value = i;
	}

	zOnSelTime() {
		this.selTime = new Date(2000, 1, 1, this.domSelHour.value, this.domSelMin.value, this.domSelSec.value, 0);
		if (this.cb != null)
			this.cb.onSelTime(this.domSelHour.value, this.domSelMin.value, this.domSelSec.value);
	}
}
/*
* Develope by KKode Pte Ltd.
* Contact: developer@kkode.com
*
* Require: Core.js, Event/Core.js, Control/Calendar.js, Control/TimeControl.js
* Usage:
* - Create a DateTimeControl object: dt = new $DateTimeControl(o);
* ++ o: option for calendar
* ++++ o.namespace: namespace added into class attribute of main control
* ++++ o.cb: callback object for event management, the callback should implement these functions
* ++++++++ onSelDateTime(selDateTime): when a time is selected
* - Add control object into DOM document: $After(pBtnCancelFrom, dt.GetDOM());
*/
class $DateTimeControl {
	constructor(o) {
		this.zResetValue();
		this.zBuildDOM();
		return this;
	}

	getDOM() {
		return this.dom;
	}

	clear() {
		this.date.$query('.sel').classList.remove('hide');
	}

	getSelDateTime() {
		return this.selDateTime;
	}

	setSelDateTime(y, m, d, h, minute, s) {
		m = m - 1;
		this.selDateTime = new Date(y, m, d, h, minute, s);
		this.refresh();
	}

	setSelNow() {
		var t = new Date();
		this.setSelDate(t.getFullYear(), t.getMonth()+1, t.getDate(), t.getHours(), t.getMinutes(), t.getSeconds());
	}

	refresh() {
		this.title.textContent = this.months[this.m] + ' ' + this.y;
		this.zBuildDate();
	}

	show() {
		this.dom.classList.remove('hide');
	}

	hide() {
		this.dom.classList.add('hide');
	}

	zResetValue() {
		this.namespace    = null;
		this.cb           = null;

		this.calendar     = null;
		this.time         = null;
		this.dom          = null;
		this.calendarDOM  = null;
		this.timeDOM      = null;

		this.selDateTime  = null;
	}

	zParseOption(o) {
		if ('namespace'    in o) this.namespace    = o.namespace;
		if ('cb'           in o) this.cb           = o.cb;
	}

	zBuildDOM() {
		var self = this,
		d = document.createElement('div');
		d.className = 'datetimecontrol' + (this.namespace ? ' ' + this.namespace : '');
		d.setAttribute('tabindex', '-1');

		this.calendar = new $Calendar({
			cb: {
				onSelDate: function() {
					self.zOnSelDateTime();
				}
			}
		});
		this.time = new $TimeControl({
			cb: {
				onSelTime: function() {
					self.zOnSelDateTime();
				}
			}
		});
		this.dom = d;
		this.calendarDOM = this.calendar.getDOM();
		this.timeDOM = this.time.getDOM();
		this.timeDOM.prepend('Time: ');
		this.dom.append(this.timeDOM);
		this.dom.append(this.calendarDOM);
	}

	zOnSelDateTime() {
		var d = this.calendar.getSelDate(),
		t = this.time.getSelTime();

		if (d == null || t == null)
			return;
		this.selDateTime = new Date(d.getFullYear(), d.getMonth(), d.getDate(), t.getHours(), t.getMinutes(), t.getSeconds(), 0);
		if (this.cb != null)
			this.cb.onSelDateTime(this.selDateTime);
	}
}
/*
* Develope by KKode Pte Ltd.
* Contact: developer@kkode.com
*
* Require: Core.js, Event/Core.js
* Usage:
* - Create a paging object: paging = new $Paging(o);
* ++ o: option for paging
* ++++ o.page: current selected day
* ++++ o.totalPage: total pages
* ++++ o.cb: callback object for event management, the callback should implement these functions
* ++++++++ onClkPage(page): when a page is clicked
* - Add paging object into DOM document: $After(pBtnCancelFrom, paging.getDOM());
*/
class $Paging {
	constructor(o) {
		this.page      = 0;
		this.totalPage = 0;
		this.dom       = null;

		this.zParseOption(o);
		this.zBuildDOM();
	}

	getDOM() {
		return this.dom;
	}

	zParseOption(o) {
		if ('page'         in o) this.page         = o.page;
		if ('totalPage'    in o) this.totalPage    = o.totalPage;
		if ('cb'           in o) this.cb           = o.cb;
	}

	zBuildDOM() {
		//If there is only 1 page, no paging control should be displayed
		if (this.totalPage < 2)
			return;

		var p = this.page,
		t = this.totalPage,
		self = this,
		i, l, r, e,
		d = document.createElement('ul');
		d.className = 'paging';
		if (p > t)
			p = t-1;
		if (t < 11) {
			l = 0;
			r = t-1;
		}
		else {
			l = p-5;
			r = p+5;
			if (l < 0) {
				r -= l;
				l = 0;
			}
			else if (r > t - 1) {
				l -= r-t+1;
				r = t-1;
			}
		}
		if (l > 0) {
			e = d.$addTxtContent('li', 'First'); e.setAttribute('data-page', 1);
			e = d.$addTxtContent('li', '...');
		}
		for (i=l; i<=r; i++) {
			e = d.$addTxtContent('li', i + 1);
			if (i + 1 == p)
				e.className = 'current';
			e.setAttribute('data-page', i+1);
		}
		if (r < t-1) {
			e = d.$addTxtContent('li', '...');
			e = d.$addTxtContent('li', 'Last'); e.setAttribute('data-page', t);
		}

		d.$on('touchstart', function(e){self.zOnTouchPaging(e);});
		d.$on('click', function(e){self.zOnClkPaging(e);});
		this.dom = d;
	}

	zOnTouchPaging(e) {
		e.preventDefault();
		var t = e.target;
		if (t.matches('li:not(.current)'))
			this.zOnClkPage(t);
	}

	zOnClkPaging(e) {
		e.preventDefault();
		var t = e.target;
		if (t.matches('li:not(.current)'))
			this.zOnClkPage(t);
	}

	zOnClkPage(e) {
		if (this.cb != null && this.cb.onClkPage != null) {
			var p = e.getAttribute('data-page');
			if (p != null)
				this.cb.onClkPage(parseInt(p));
		}
	}
}
class AudioControl {
	constructor(c) {
	this.audio = c;

	this.zBuild();
}

	onEvt(e, f) {
		this.audio.$on(e, f);
	}

	togglePlay() {
		this.zOnClkBtn();
	}

	zBuild() {
		var b, i, d = document.createElement('div');
		d.className = 'audio';
		b = d.$add('button');
		b.className = 'audiobtn';
		i = b.$add('i');
		i.className = 'fa fa-play'; i.setAttribute('aria-hidden', 'true');
		this.control = d;
		d = d.$add('div');
		d.className = 'duration';
		d = d.$add('div');
		d.className = 'full';
		d = d.$add('div');
		d.className = 'running'; d.style.width = '0%';

		this.btn = this.control.$query('.audiobtn');
		this.full = this.control.$query('.full');
		this.running = this.control.$query('.running');
		$After(this.audio, this.control);
		this.audio.classList.add('hide');

		var self = this;
		this.btn.$on('click', function(){self.zOnClkBtn();});
		this.full.$on('click', function(e){self.zOnSeek(e);});
		this.audio.$on('timeupdate', function(){self.zOnTimeUpdate();});
		this.audio.$on('ended', function(){self.zOnEnded();});
	}

	zOnClkBtn() {
		var d = document.createElement('i');
		d.setAttribute('aria-hidden', 'true');
		if (this.audio.paused) {
			this.audio.play();
			d.className = 'fa fa-pause';
		} else {
			this.audio.pause();
			d.className = 'fa fa-play';
		}
		this.btn.$clrChild();
		this.btn.appendChild(d);
	}

	zOnSeek(e) {
		var r = this.full.getBoundingClientRect();
		this.audio.currentTime = this.audio.duration*(e.pageX-r.left)/r.width;
	}

	zOnTimeUpdate() {
		this.running.style.width = (this.audio.currentTime*100)/this.audio.duration + '%';
	}

	zOnEnded() {
		var d = document.createElement('i');
		d.setAttribute('aria-hidden', 'true');
		d.className = 'fa fa-play';
		this.btn.$clrChild();
		this.btn.appendChild(d);
	}
}
/*
* Develope by katapema@yahoo.com
*
* Require: Core.js, Event/Core.js
* Usage:
* - Create a Tab object: tab = new $Tab(o);
* ++ o: option for Tab
* ++++ o.element: the root/container HTML element for the tab UI
*/
class $Tab {
	constructor(o) {
		this.tab = o.element;
		this.tablist = this.tab.children[0];
		this.selectedTab = this.tablist.$query('.selected');
		this.selectedTabDiv = $id(this.selectedTab.getAttribute('fortab'));

		var self = this;
		this.tablist.$on('click', function (e) { self.zOnClkTabList(e); });
	}

	zFindSelectedTab() {
	}

	zOnClkTabList(e) {
		var fortab = e.target.getAttribute('fortab');
		if (fortab == null || !fortab.length)
			return;
		fortab = $d.$id(fortab);

		this.selectedTab.classList.remove('selected');
		this.selectedTabDiv.classList.add('hide');

		this.selectedTab = e.target;
		this.selectedTabDiv = fortab;

		this.selectedTab.classList.add('selected');
		this.selectedTabDiv.classList.remove('hide');
	}
}
/*
* Develope by KKode Pte Ltd.
* Contact: developer@kkode.com
*/
/**
* @param n Name of the cookie
* @param v Value of the cookie
* @param i Expire interval of cookie in milliseconds
* @param p Path of the cookie
* @param d Domain of the cookie
*/
function $CookieSet(n, v, i, p, d) {
	var e = "";
	if (i) {
		var t = new Date();
		t.setTime(t.getTime() + i);
		e = "; expires=" + t.toGMTString();
	}
	document.cookie = escape(n) + "=" + escape(v) + "; Secure" + e + (d ? "; domain=" + d : "") + (p ? "; path=" + p : "");
}

/**
* @param n Name of the cookie
* @param d Default value if cookie is not set
*/
function $CookieGet(n, d) {
	return unescape(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + escape(n).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || d;
}

/**
* @param n Name of the cookie
* @param p Path of the cookie
*/
function $CookieDel(n, p) {
    document.cookie = escape(n) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" + (p ? "; path=" + p : "");
}/*
* Develope by KKode Pte Ltd.
* Contact: developer@kkode.com
*/

/**
* Detect whether the browser is touchable
* @return true if the screen is touchable. Otherwise, return false
*/
let $bIsTouchable = 'ontouchstart' in document.documentElement;