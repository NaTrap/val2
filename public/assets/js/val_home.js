
"use strict";class Main {
	constructor() {
		this.btnStart = document.getElementById('btnStart')
		this.dialog = document.getElementById('dialog')
		this.audio = null;

		this.streaming = false;
		this.ws = null;
		// this.btnStart.addEventListener('click', (e) => {
		// 	e.preventDefault();
		// 	// if (this.btnStart.innerHTML == 'Start') {
		// 		this.streamStart();
		// 		// this.btnStart.innerHTML = 'Stop';
		// 	// }
		// 	// else {
		// 	// 	this.streamStop();
		// 	// 	// this.btnStart.innerHTML = 'Start';
		// 	// }
		// });

		if (window.AudioContext)
			this.audioCtx = new AudioContext;
		else if (window.webkitAudioContext)
			this.audioCtx = new webkitAudioContext;

		this.ws = new WebSocket("wss://robotval.kkode.com/ws/stream");
		this.ws.onopen = () => {
			navigator.mediaDevices.getUserMedia({ audio: true })
				.then((stream) => {

					//Bug in mobile chrome cause the onaudioprocess not fire. We need to create a fake bufferSource and start it
					//in order to let it fire
					this.bufferSource = this.audioCtx.createBufferSource();
					this.bufferSource.buffer = this.audioCtx.createBuffer(1, 4096, 48000);
					this.bufferSource.connect(this.audioCtx.destination);
					this.bufferSource.start(0);
					this.bufferSource.stop();

					this.streamSource = this.audioCtx.createMediaStreamSource(stream);
					this.scriptProcessor = this.audioCtx.createScriptProcessor(4096, 1, 1);
					this.scriptProcessor.onaudioprocess = (e) => {
						if (this.streaming) {
							if (!this.audio || this.audio.ended) {
								let rawAudio = e.inputBuffer.getChannelData(0);
								let l = rawAudio.length;
								let audio = new Int16Array(l + 1);
								audio[l] = 1;
								while (l--)
									audio[l] = Math.min(1, rawAudio[l]) * 0x7FFF;
								this.ws.send(audio.buffer);
							}
						}
					};
					this.streamSource.connect(this.scriptProcessor);
					this.scriptProcessor.connect(this.audioCtx.destination);
				})
				.catch((e) => {
					this.asrResult.innerHTML = 'Unable to open microphone';
				});
				setTimeout(() =>{
					console.log('aaa')
					this.streamStart();
				},2000)
		};
		this.ws.onerror = (e) => {
			console.log(e)
			alert("Websocket error" + JSON.stringify(e));
		};
		this.ws.onclose = (e) => {
			console.log('websocket closeaa')
			console.log(e.code)
			console.log(e.reason)
			console.log(e.wasClean)
			this.streaming = false;
		};
		this.ws.onmessage = (e) => {
			let r = JSON.parse(e.data);
			if (r.msg == 'tts') {
				if(window.responsiveVoice.isPlaying() ){
					window.responsiveVoice.cancel();
				}
				this.streamStop();
				window.responsiveVoice.speak(r.text,'US English Female',{rate: 0.9, onend:() => {
					this.streamStart();
				}})
				// this.audio = new Audio('https://robotval.kkode.com/tts/play/' + r.file);
				// this.audio.play();
			}
			else if (r.msg == 'asr') {
			}
		};
	}

	streamStart() {
		let cmd = new Int16Array(1);
		cmd[0] = 0;
		this.ws.send(cmd.buffer);
		this.streaming = true;
	}

	stream() {
		if (this.streaming) {
			let rawAudio = e.inputBuffer.getChannelData(0);
			let l = rawAudio.length;
			let audio = new Int16Array(l + 1);
			audio[l] = 1;
			while (l--)
				audio[l] = Math.min(1, rawAudio[l]) * 0x7FFF;
			this.ws.send(audio.buffer);
		}
	}

	streamStop() {
		this.streaming = false;
		let cmd = new Int16Array(1);
		cmd[0] = 2;
		this.ws.send(cmd.buffer);
		// this.ws.close();
		console.log('websocket close')
	}
}