<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/ocr_scan','OCRController@scan');
Route::get('/tes', function(){
    $ip =   "127.0.0.1";
    exec("ping -n 3 $ip", $output, $status);
});

Route::get('/scan1', function(){
    $command = "libSK_380_R_TEST_new 6";
    exec("./$command", $output, $status);
});
// Route::get('/tes', function(){
//     $command = "127.0.0.1";
//     exec("ping -n 3 $command", $output, $status);
//     print_r($output);
// });
Route::get('/card1', function(){
    $dir = "assets/files/";
    $command = "libAD1_1Driver_Test_new 5";
    exec("cd $dir && ./$command", $output, $status);
    print_r($output);
});
Route::get('/scan', function(){
    $dir = "assets/files/";
    $command = "libSK_380_R_TEST_new";
    exec("cd $dir && ./$command", $output, $status);
    print_r($output);
});
Route::get('/card2', function(){
    $dir = "assets/files/";
    $command = "libAD1_1Driver_Test_new 6";
    exec("cd $dir && ./$command", $output, $status);
    print_r($output);
});