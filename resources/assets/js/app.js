require ('./bootstrap');
import VueRouter from 'vue-router';
import axios from 'axios'
import VueAxios from 'vue-axios'


window.Vue = require('vue');

Vue.use(VueRouter, VueAxios, axios);


const dashboard = require('./components/Dashboard').default;
const lang = require('./components/Lang').default;
const home = require('./components/Home').default;
const feedback = require('./components/Feedback').default;
const video = require('./components/Video').default;
const checkin = require('./components/Checkin').default;
const checkin2 = require('./components/Checkin2').default;
const checkin3 = require('./components/Checkin3').default;
// const checkin4 = require('./components/Checkin4').default;
const checkin5 = require('./components/Checkin5').default;
const checkin6 = require('./components/Checkin6').default;
const checkin7 = require('./components/Checkin7').default;
const print = require('./components/Print').default;
const mobile = require('./components/Mobile').default;
const mobile2 = require('./components/Mobile2').default;
const mobile3 = require('./components/Mobile3').default;
const routes = [
	{
		path: '/',
		component: dashboard
	},
	{
		path: '/lang',
		component: lang
	},
	{
		path: '/home',
		component: home
	},
	{
		path: '/video',
		component: video
	},
	{
		path: '/feedback',
		component: feedback
	},
	{
		path: '/check-in',
		component: checkin,
	},
	{
		path: '/check-in2',
		component: checkin2,
		props:true,
		name:'check-in2'
	},
	{
		path: '/check-in3',
		component: checkin3
	},
	// {
	// 	path: '/check-in4',
	// 	component: checkin4
	// },
	{
		path: '/check-in5',
		component: checkin5
	},
	{
		path: '/check-in6',
		component: checkin6
	},
	{
		path: '/check-in7',
		component: checkin7
	},
	{
		path: '/print',
		component: print
	},
	{
		path: '/mobile',
		component: mobile,
		props:true,
		name:'mobile'
	},
	{
		path: '/mobile2',
		component: mobile2,
		props:true,
		name:'mobile2'
	},
	{
		path: '/mobile3',
		component: mobile3
	}
]

const router = new VueRouter({
	routes,
})

const app = new Vue({
	el:'#app',
	router
})